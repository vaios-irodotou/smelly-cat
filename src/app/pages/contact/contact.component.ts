import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { mergeMap,of } from 'rxjs';

import { UserService } from 'src/app/core/data/user.service';
import { Validators } from '@angular/forms';
 
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup
  locationData: any

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {

    this.contactForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      city: [''],
      postalCode: [''],
      address: [''],
      message: [''],
      termsAndConditions: [false, Validators.requiredTrue]
    });
   }

  get fullName() {
    return this.contactForm.get('fullName')
  }

  get email(): any {
    return this.contactForm.get('email')
  }

  get city() {
    return this.contactForm.get('city')
  }

  get postalCode() {
    return this.contactForm.get('postalCode')
  }

  get address() {
    return this.contactForm.get('address')
  }

  get message() {
    return this.contactForm.get('message')
  }

  get termsAndConditions() {
    return this.contactForm.get('termsAndConditions')
  }


  ngOnInit(): void {

    this.userService.getLocation().pipe(
      mergeMap((locationResponse: any) => {
        const locationData = {
          latitude: locationResponse.coords.latitude,
          longitude: locationResponse.coords.longitude
        };
        return of(locationData);
      })
    ).subscribe((locationData) => {
      console.log(locationData);
      this.locationData = locationData;
      console.log(this.locationData)
    });

  }

  onSubmit(formData: any) {
    console.log(formData.value)
    // this.apiService.sendForm(formData.value).subscribe((res: any) =>{

    // })
  }

}
