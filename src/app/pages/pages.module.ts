import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { CoreModule } from '../core/core.module';
import { AdoptKittensComponent } from './adopt-kittens/adopt-kittens.component';
import { KittyStoriesComponent } from './kitty-stories/kitty-stories.component';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';
import { PagesComponent } from './pages.component';



@NgModule({
  declarations: [
    AdoptKittensComponent,
    KittyStoriesComponent,
    BlogComponent,
    ContactComponent,
    PagesComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    CoreModule,
  ]
})
export class PagesModule { }
