import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
// import { AgmCoreModule, AgmMap } from '@agm/core';
// import { environment } from 'src/environments/environment';



@NgModule({
  declarations: [
    ContactFormComponent
  ],
  imports: [
    CommonModule,
    ContactRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    // AgmCoreModule.forRoot({
    //   apiKey: environment.googleApiKey
    // })
  ]
})
export class ContactModule { }
