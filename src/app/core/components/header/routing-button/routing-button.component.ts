import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'routing-button',
  templateUrl: './routing-button.component.html',
  styleUrls: ['./routing-button.component.scss']
})
export class RoutingButtonComponent implements OnInit {

  constructor() { }

  @Input() title: string = ''

  ngOnInit(): void {
  }

}
