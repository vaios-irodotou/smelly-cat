import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdoptKittensRoutingModule } from './adopt-kittens-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdoptKittensRoutingModule
  ]
})
export class AdoptKittensModule { }
