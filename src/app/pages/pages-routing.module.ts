import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { AdoptKittensComponent } from './adopt-kittens/adopt-kittens.component';
import { BlogComponent } from './blog/blog.component';
import { KittyStoriesComponent } from './kitty-stories/kitty-stories.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  { 
    path: '',
    component: PagesComponent,
    children: [
      { path:'adopt-kittens', component: AdoptKittensComponent},
      { path:'kitty-stories', component: KittyStoriesComponent},
      { path: 'about-us', component: AboutUsComponent},
      { path: 'blog', component: BlogComponent},
      {
        path: 'contact',
        loadChildren: () => import('./contact/contact.module').then( m => m.ContactModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }