import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer.component';
import { SocialsComponent } from './socials/socials.component';
import { FooterLinksComponent } from './footer-links/footer-links.component';
import { CopyrightComponent } from './copyright/copyright.component';



@NgModule({
  declarations: [
    FooterComponent,
    SocialsComponent,
    FooterLinksComponent,
    CopyrightComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FooterComponent
  ]
})
export class FooterModule { }