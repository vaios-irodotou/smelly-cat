import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from './components/header/header.module';
import { FooterModule } from './components/footer/footer.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    FooterModule,

    
  ],
  exports: [
    HeaderModule,
    FooterModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CoreModule { }