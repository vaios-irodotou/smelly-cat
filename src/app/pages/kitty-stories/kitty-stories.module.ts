import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KittyStoriesRoutingModule } from './kitty-stories-routing.module';
import { CoreModule } from 'src/app/core/core.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    KittyStoriesRoutingModule,
    CoreModule
  ]
})
export class KittyStoriesModule { }
