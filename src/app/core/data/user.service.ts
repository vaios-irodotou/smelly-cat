import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }



  public getLocation(): Observable<any> {
    return new Observable((observer: any) => {
      if (window.navigator && window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(
          (position: any) => {
            observer.next(position);
            observer.complete();
          }
        )
      } else {
        observer.error('Unsupported Browser')
      }
    })
  }



}
