import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router: Router
  ) { }
  baseUrl = environment.baseUrl


  pages = [
    {name: 'Adopt kittens', url: '/pages/adopt-kittens'},
    {name: 'Kitty Stories', url: '/pages/kitty-stories'},
    {name: 'About us', url: '/pages/about-us'},
    {name: 'Blog', url: '/pages/blog'},
    {name: 'Contact', url: '/pages/contact'}
  ]
  navigateToPage(route: string) {
    console.log(route);
    this.router.navigate([route]);
  }

  ngOnInit(): void {
    console.log('kek')
  }
  
}
