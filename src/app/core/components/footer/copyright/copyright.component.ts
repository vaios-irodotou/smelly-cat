import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-copyright',
  templateUrl: './copyright.component.html',
  styleUrls: ['./copyright.component.scss']
})
export class CopyrightComponent implements OnInit {

  copyrightText:string = 'Copyright © SmellyCat 2020'

  constructor() { }

  ngOnInit(): void {
  }

}
