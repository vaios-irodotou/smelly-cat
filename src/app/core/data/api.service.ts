import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = environment.api.endpoints.base
  private formUrl = environment.api.endpoints.form



  constructor(
    private http: HttpClient
  ) { }

  sendForm(formData: any): Observable<any> {
    let url = this.apiUrl + this.formUrl
    return this.http.post(url, formData, httpOptions).pipe()
  }
}
